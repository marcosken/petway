# PetWay

<p align="center">
  <img src="./assets/logo_petway.png" alt="Logo PetWay"/>
</p>

## 💻 Sobre o Projeto

Este projeto foi desenvolvido como atividade final (Capstone) do framework Django do curso de Desenvolvimento Web FullStack da Kenzie Academy Brasil.

A plataforma Pet Way tem como objetivo unir os donos de pets com prestadores de serviço.

De um lado temos os donos que não podem ou não tem tempo para passear com seus pets, ou irão viajar mas querem alguém que fique de olho neles. Temos situações que o pet é desobediente ou queira aprender algum truque. Também temos casos em que é preciso levar ou buscar o pet para algum lugar.

Por outro lado temos os prestadores de serviços que podem atuar como dogwalker, petsitter, adestramento e táxi.

Assim, a plataforma facilita a divulgação dos serviços bem como a procura por profissionais, não se restringindo às redes sociais.

## 💡 Funcionalidades

- Cadastro de usuário
- Cadastro de pet
- Inserção de redes sociais do prestador
- Cadastro dos serviços prestados (Pet sitter, Pet walker, Pet trainer e Pet taxi)
- Criação da ordem de serviço pelo dono do pet
- Avaliação do serviço, pelo dono do pet e pelo prestador
- Cadastro de animais para adoção

## ℹ️ Informações

- URL base da API: https://petway-api.herokuapp.com/api

- Documentação da API:

  - [Swagger](https://petway-api.herokuapp.com/)
  - [ReDoc](https://petway-api.herokuapp.com/redoc/)

- JSON para utilizar no Insomnia:
  [![Run in Insomnia}](https://insomnia.rest/images/run.svg)](https://insomnia.rest/run/?label=PetWay%20Insomnia&uri=https%3A%2F%2Fgitlab.com%2Fmarcosken%2Fpetway%2F-%2Fraw%2Fmain%2Fassets%2FPetWay_Insomnia.json)

## 📊 Diagrama ER

![Diagrama ER](./assets/diagrama_er.png)

## 🛠 Tecnologias

Para o desenvolvimento desse projeto, foi necessária a utilização das seguintes ferramentas:

- [Django](https://www.djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [Python](https://www.python.org/)

## 👥 Autores

- João Pedro da Silva Santos [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=plastic&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/jo%C3%A3o-pedro-dev/)
- Jorge Willian Paez Nagakura [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=plastic&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/jorgenagakura/)
- Marcos Kenji Kuribayashi [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=plastic&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/marcos-kuribayashi/)
- Mein Mieko Chang [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=plastic&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/meinmiekochang/)
- Miqueias Carvalho dos Santos [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=plastic&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/miqueias-carvalho-dos-santos/)
