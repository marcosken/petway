# Generated by Django 4.0.3 on 2022-04-01 23:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='pass_recovery_token',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
