from django.urls import path
from .views import AddressListCreateView, AddressListView, AddressUpdateView, AddressListProvidersView

urlpatterns = [
    path('addresses/search/', AddressListProvidersView.as_view()),
    path('addresses/users/', AddressListView.as_view()),
    path('addresses/', AddressListCreateView.as_view()),
    path('addresses/<str:address_id>/', AddressUpdateView.as_view()),
]
